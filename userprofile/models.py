from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
# 引入内置信号
from django.db.models.signals import post_save
# 引入信号接收器的装饰器
from django.dispatch import receiver



class Profile(models.Model):
    """用户信息

    每个用户都与django自带的基本用户信息有级联关系

    属性：
        user     ：django自带的用户类信息，包含姓名、密码等
        phone    ：用户的电话号码
        avatar   ：用户的头像，按照上传年月日保存至avatar文件路径中
        bio      ：用户的个人简介
        email    ：用户的邮箱
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    phone = models.CharField(max_length=20, blank=True)
    avatar = models.ImageField(upload_to='avatar/%Y%m%d/', blank=True)
    bio = models.TextField(max_length=500, blank=True)
    email = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return 'user {}'.format(self.user.username)