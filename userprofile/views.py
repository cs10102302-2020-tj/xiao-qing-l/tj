from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login , logout
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.hashers import check_password
from django.contrib.contenttypes.models import ContentType
from .models import Profile
from .forms import UserLoginForm , UserRegisterForm, UserProfileForm, UserPasswordForm
from like.models import LikeRecord
from collect.models import CollectRecord
from article.models import ArticlePost
from question.models import QuestionPost
from comment.models import Comment
from answer.models import Answer
# 引入分页模块
from django.core.paginator import Paginator
# 引入权限检查模块
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission

# 函数名：  search_user
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    根据前端获得的search值进行对用户的搜索
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          'request'  : 前端请求类型
#          'users'    : 搜索到的用户
#          'search_u' : 前端输入的搜索内容
#          'search/search_user.html' : 用户搜索界面
# 修改记录：
def search_user(request):
    # 判断是否进行了搜索
    search = request.GET.get('search')
    # 如果进行了搜索
    if search:
        users = User.objects.filter(
            # 匹配用户名(不区分大小写)
            Q(username__icontains=search)
        )
        context = {'users': users, 'search_u': search}
    # 如果未进行搜索
    else:
        context = {'users': [], 'search_u': search}
    return render(request, 'search/search_user.html', context)

# 函数名：  user_login
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    根据前端获得的用户登录表格信息，进行登录处理操作（包含密码检查）
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          'request'  : 前端请求类型
#          'form'     : 用户登录所需要的信息表格
#          '/'        : 登陆成功返回主页    
#          'userprofile/login.html' : 用户登录界面
# 修改记录：
def user_login(request):
    # 用户进行数据POST
    if request.method == 'POST':
        # 得到用户的数据
        user_login_form = UserLoginForm(data=request.POST)
        # 判断数据是否有效
        if user_login_form.is_valid():
            # 清洗出合法数据
            data = user_login_form.cleaned_data
            # 检验账号、密码是否正确匹配数据库中的某个用户
            # 如果均匹配则返回这个 user 对象
            user = authenticate(username=data['username'], password=data['password'])
            if user:
                # 将用户数据保存在 session 中，即实现了登录动作
                login(request, user)
                # 返回主页链接
                return redirect("/")
            else:
                # 创建用户表单类
                user_login_form = UserLoginForm()
                # 赋予上下文
                context = {'form': user_login_form}
                messages.success(request, "账号或密码输入不合法")
                return render(request, 'userprofile/login.html', context)
        else:
            # 创建用户表单类
            user_login_form = UserLoginForm()
            # 赋予上下文
            context = {'form': user_login_form}
            messages.success(request, "账号或密码输入不合法")
            return render(request, 'userprofile/login.html', context)
    # 用户获取视图
    elif request.method == 'GET':
        # 创建用户表单类
        user_login_form = UserLoginForm()
        # 赋予上下文
        context = { 'form': user_login_form }
        # 返回呈现给登录页面
        return render(request, 'userprofile/login.html', context)
    # 危险情况
    else:
        # 创建用户表单类
        user_login_form = UserLoginForm()
        # 赋予上下文
        context = {'form': user_login_form}
        messages.success(request, "请使用GET或POST请求数据")
        return render(request, 'userprofile/login.html', context)

# 函数名：  user_logout
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    将以登录的用户执行登出操作
# 输入参数：request Django前端传来的请求
# 返回值：  redirect函数
#          '/'  : 临时重定向到主页
# 修改记录：
def user_logout(request):
    # 用户登出
    logout(request)
    # 返回首页
    return redirect("/")

# 函数名：  user_register
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    根据前端获得的用户注册表格信息，进行注册录处理操作（包含密码检查，用户权限授予）
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          'request'  : 前端请求类型
#          'form'     : 用户注册所需要的信息表格
#          '/'        : 登陆成功返回主页    
#          'userprofile/register.html' : 用户注册界面
# 修改记录：
def user_register(request):
    # 用户进行数据POST
    if request.method == 'POST':
        # 得到注册表单数据
        user_register_form = UserRegisterForm(data=request.POST)
        # 检测数据是否有效
        if user_register_form.is_valid():
            # 保存表单到暂存区
            new_user = user_register_form.save(commit=False)
            # 设置密码
            new_user.set_password(user_register_form.cleaned_data['password'])
            # 保存用户名密码到数据库
            new_user.save()
            # 保存好数据后立即登录
            login(request, new_user)
            # 赋予新用户权限
            user_authorization(new_user.id)
            # 返回主界面
            return redirect("/")
        else:
            # 获取注册表格初始化
            user_register_form = UserRegisterForm()
            # 联系上下文
            context = {'form': user_register_form}
            messages.success(request, "注册表单有误，请重新输入~")
            # 传输到模板呈现给用户
            return render(request, 'userprofile/register.html', context)
    # 用户进行表格页面请求
    elif request.method == 'GET':
        # 获取注册表格初始化
        user_register_form = UserRegisterForm()
        # 联系上下文
        context = { 'form': user_register_form }
        # 传输到模板呈现给用户
        return render(request, 'userprofile/register.html', context)
    else:
        # 获取注册表格初始化
        user_register_form = UserRegisterForm()
        # 联系上下文
        context = {'form': user_register_form}
        messages.success(request, "请使用GET或POST请求数据")
        # 传输到模板呈现给用户
        return render(request, 'userprofile/register.html', context)


# 函数名：  user_delete
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    对已登录的用户进行删除操作
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  redirect函数、HttpResponse网站回执
#          '/'  : 临时重定向到主页
# 修改记录：
@login_required(login_url='/userprofile/login/')
def user_delete(request, id):
    # 得到用户id
    user = User.objects.get(id=id)
    # 验证登录用户、待删除用户是否相同
    if request.user == user:
        # 退出登录
        logout(request)
        # 删除数据
        user.delete()
        # 返回主界面
        return redirect("/")
    else:
        return HttpResponse("你没有删除操作的权限。")

# 函数名：  profile
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    查看用户个人信息界面，将个人信息返回至前端
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  类型（JsonResponse）
#          'request'  : 前端请求类型
#          'profile'  : 用户简历中的扩展信息
#          'user'     : 用户的基本信息
#          'userprofile/personaldetail.html' : 用户的个人信息界面
# 修改记录：
@login_required(login_url='/userprofile/login/')
def profile(request, id):
    user = User.objects.get(id=id)
    if Profile.objects.filter(user_id=id).exists():
        profile = Profile.objects.get(user_id=id)
    else:
        profile = Profile.objects.create(user=user)

    context = {'profile': profile, 'user': user}
    return render(request, 'userprofile/personaldetail.html',context)

# 函数名：  profile_update
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    更新用户的个人信息
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  类型（JsonResponse）、redirect函数、HttpResponse网站回执
#          'request'      : 前端请求类型
#          'profile_form' : 用户编辑个人信息所需要的信息表格
#          'profile'      : 用户简历中的扩展信息
#          'user'         : 用户的基本信息
#          'id'           : 用户的id
#          'userprofile:profile'      : 重定向到用户的个人信息编辑界面
#          'userprofile/profile.html' : 用户的个人信息编辑界面
# 修改记录：
@login_required(login_url='/userprofile/login/')
def profile_update(request, id):
    user = User.objects.get(id=id)
    # user_id 是 OneToOneField 自动生成的字段
    if Profile.objects.filter(user_id=id).exists():
        profile = Profile.objects.get(user_id=id)
    else:
        profile = Profile.objects.create(user=user)

    if request.method == 'POST':
        # 验证修改数据者，是否为用户本人
        if request.user != user:
            return HttpResponse("你没有权限修改此用户信息。")

        profile_form = UserProfileForm(request.POST,request.FILES)
        if profile_form.is_valid():
            # 取得清洗后的合法数据
            profile_cd = profile_form.cleaned_data
            profile.phone = profile_cd['phone']
            profile.bio = profile_cd['bio']
            profile.email = profile_cd['email']
            user.email = profile_cd['email']
            if 'avatar' in request.FILES:
                profile.avatar=profile_cd['avatar']
            user.save()
            profile.save()
            # 带参数的 redirect()
            return redirect("userprofile:profile", id=id)
        else:
            return HttpResponse("注册表单输入有误。请重新输入~")

    elif request.method == 'GET':
        profile_form = UserProfileForm()
        context = {'profile_form': profile_form, 'profile': profile, 'user': user}
        return render(request, 'userprofile/profile.html', context)
    else:
        return HttpResponse("请使用GET或POST请求数据")

# 函数名：  password_update
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    更改用户的密码
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  类型（JsonResponse）、redirect函数
#          'request'            : 前端请求类型
#          'user_password_form' : 用户更改个人密码所需要的信息表格
#          '/'                  : 临时重定向到主页
#          'userprofile/setting.html' : 用户更改密码界面
# 修改记录：
@login_required(login_url='/userprofile/login/')
def password_update(request, id):
    user = get_object_or_404(User, id=id)

    if request.method == "POST":
        user_password_form = UserPasswordForm(request.POST)

        if user_password_form.is_valid():
            old_password = user_password_form.cleaned_data['old_password']
            new_password = user_password_form.cleaned_data['new_password']
            new_password2 = user_password_form.cleaned_data['new_password2']

            if not check_password(old_password, user.password):
                messages.error(request, "原始密码输入错误！")
                return render(request, 'userprofile/setting.html', {'user_password_form': user_password_form})

            if new_password != new_password2:
                messages.error(request, "两次新密码输入不同！")
                return render(request, 'userprofile/setting.html', {'user_password_form': user_password_form})

            user.set_password(new_password)
            user.save()
            logout(request)

            messages.success(request, "修改密码成功！")
            return redirect("/")
    else:
        user_password_form = UserPasswordForm()
        return render(request, 'userprofile/setting.html', {'user_password_form': user_password_form})

# 函数名：  get_collect_record
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    用户查看其收藏的信息，根据情况作出相应的数据返回
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  类型（JsonResponse）、redirect函数、HttpResponse网站回执
#          'request'      : 前端请求类型
#          'collections'  : 用户收藏的文章问题列表
#          'len'          : 用户收藏数量
#          'search'       : 用户若是进行了搜索，则返回搜索的关键词
#          'userprofile/collection.html' : 用户的收藏界面
# 修改记录：
@login_required(login_url='/userprofile/login/')
def get_collect_record(request, id):
    # 判断是否进行了搜索
    if request.user.id != id:
        return HttpResponse("你没有权限查看收藏信息。")
    search = request.GET.get('search')
    collect_records = CollectRecord.objects.filter(user=id)
    collections = []
    for collect_record in collect_records:
        # print(collect_record.content_type)
        # print(collect_record.object_id)
        try:
            object = collect_record.content_type.get_object_for_this_type(id=collect_record.object_id)
            if search:
                if collect_record.content_type.model == 'articlepost' and (search in object.title or search in object.body):
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'questionpost' and (search in object.title or search in object.description):
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'comment' and search in object.body:
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'answer' and search in object.body:
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
            else:
                collections.append({'content_type': collect_record.content_type.model, 'object': object})
        except:
            print("query doesn't match error")

    length = len(collections)
    return render(request, 'userprofile/collection.html', {'collections': collections, 'len': length, 'search': search})

# 函数名：  user_authorization
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    对用户进行操作授权
# 输入参数：id  : 用户的id
# 返回值：  无
# 修改记录：
def user_authorization(id):
    user = User.objects.get(id=id)
    # 查找权限
    p1 = Permission.objects.get(codename = 'add_articlepost')
    p2 = Permission.objects.get(codename = 'change_articlepost')
    p3 = Permission.objects.get(codename = 'view_articlepost')
    p4 = Permission.objects.get(codename = 'add_questionpost')
    p5 = Permission.objects.get(codename = 'change_questionpost')
    p6 = Permission.objects.get(codename = 'view_questionpost')
    p7 = Permission.objects.get(codename = 'add_comment')
    p8 = Permission.objects.get(codename = 'change_comment')
    # 由于有重复字段，所以不能靠codename查询，用序号代替，所以如果新作数据迁移可能需要改这个地方
    #p9 = Permission.objects.get(codename = 'add_answer')
    # 序号重复，暂用其他权限名称
    # p9 = Permission.objects.get(id = 41)
    p9 = Permission.objects.get(codename = 'add_session')
    #p10 = Permission.objects.get(codename = 'change_answer')
    # p10 = Permission.objects.get(id = 42)
    p10 = Permission.objects.get(codename = 'change_session')
    p11 = Permission.objects.get(codename = 'change_collectcount')
    p12 = Permission.objects.get(codename = 'change_likecount')            
    user.user_permissions.set([p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12])



# 函数名：  user_forbidden
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    管理员可以对用户的权限进行封禁
# 输入参数：request Django前端传来的请求
#          id  : 用户的id
# 返回值：  redirect函数、HttpResponse网站回执
#          '/' : 临时重定向到主页
# 修改记录：
@login_required(login_url='/userprofile/login/')
def user_forbidden(request, id):
    nowuser = User.objects.get(id=request.user.id)

    if not nowuser.has_perm('userprofile.user_change'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    # 取出对象用户
    myuser = User.objects.get(id=id)

    # 赋予新用户权限
    p1 = Permission.objects.get(codename = 'add_articlepost')
    p2 = Permission.objects.get(codename = 'change_articlepost')
    p3 = Permission.objects.get(codename = 'view_articlepost')
    p4 = Permission.objects.get(codename = 'add_questionpost')
    p5 = Permission.objects.get(codename = 'change_questionpost')
    p6 = Permission.objects.get(codename = 'view_questionpost')
    p7 = Permission.objects.get(codename = 'add_comment')
    p8 = Permission.objects.get(codename = 'change_comment')
    p9 = Permission.objects.get(codename = 'add_session')
    p10 = Permission.objects.get(codename = 'change_session')
    p11 = Permission.objects.get(codename = 'change_collectcount')
    p12 = Permission.objects.get(codename = 'change_likecount')            
    myuser.user_permissions.remove(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12)
    messages.success(request, "封禁用户成功！")
    return redirect("/")

# 函数名：  get_profile
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    根据前端需求，返回当前登录用户的个人信息作为全局数据
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          'request_1'  : 前端请求类型
#          'globle_profile'    : 搜索到的用户
# 修改记录：
def get_profile(request):
    if(request.user.is_authenticated):
        if Profile.objects.filter(user_id=request.user.id).exists():
            profile = Profile.objects.get(user_id=request.user.id)
        else:
            profile = Profile.objects.create(user=request.user)


        context = {'globle_profile': profile}
        return  context
    return {'request1': request}

# 函数名：  get_myob
# 作者：    tyt
# 日期：    2020-7-22
# 功能：    用户查看自己发表的文章和问题，返回相关数据
# 输入参数：request Django前端传来的请求
#          id : 用户的id
# 返回值：  类型（JsonResponse）
#          'title'           : 用户发表的文章/问题的标题
#          'id'              : 用户发表的文章/问题的序号
#          'column'          : 用户发表的文章/问题的所属板块
#          'updated'         : 用户发表的文章/问题的更新时间
#          'body'            : 用户发表的文章/问题的内容
#          'type'            : 用户发表的信息的类型（文章、问题）
#          'column_content'  : 用户发表的问题、文章列表
#          'this_user'       : 当前页面的用户的个人信息
#          'userprofile/myob.html' : 用户个人发表动态页面
# 修改记录：
def get_myob(request,id):
    # 判断是否进行了搜索
    user = User.objects.get(id=id)
    questions = QuestionPost.objects.filter(author=id)
    articles = ArticlePost.objects.filter(author=id)
    column_content = []
    for question in questions:
        content = { 'title': question.title, 'id': question.id, 'column': str(question.column),
                   'updated': question.updated, 'body': question.description, 'type': 'question'}
        column_content.append(content)
        pass
    for article in articles:
        content = {'title': article.title, 'id': article.id, 'column': str(article.column), 'updated': article.updated,
                  'body': article.body, 'type': 'article'}
        column_content.append(content)
        pass

    column_content = sorted(column_content, key=lambda x: (x['updated']), reverse=True)

    paginator = Paginator(column_content, 5)
    # 获取 url 中的页码
    page = request.GET.get('page')
    # 将导航对象相应的页码内容返回给 articles
    column_content = paginator.get_page(page)

    column_content = {'column_content': column_content, 'this_user': user}
    return render(request, 'userprofile/myob.html', column_content)
