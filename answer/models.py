from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericRelation
from question.models import QuestionPost
from like.models import LikeRecord
from collect.models import CollectRecord
from mptt.models import MPTTModel, TreeForeignKey


# 博文的回答
class Answer(models.Model):
    question = models.ForeignKey(
        QuestionPost,
        on_delete=models.CASCADE,
        related_name='answers'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='answers'
    )
    body = models.TextField()
    created = models.DateTimeField(default=timezone.now)

    file = models.FileField(upload_to='answer_file/%Y%m%d/', blank=True)

    like_records = GenericRelation(LikeRecord, related_query_name='answers')
    # 收藏记录
    collect_records = GenericRelation(CollectRecord, related_query_name='answers')

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return self.body[:20]

    # 获得回答详细页面url
    def get_absolute_url(self):
        return reverse('question:question_detail', args=[self.question.id])

    # 回答是否点赞
    def answer_is_liked(self, user):
        like_records = self.like_records.filter(user=user)
        return len(like_records) != 0

    # 回答是否收藏
    def answer_is_collected(self, user):
        collect_records = self.collect_records.filter(user=user)
        return len(collect_records) != 0

    # 回答创建时间是否错误
    def was_created_recently(self):
        diff = timezone.now() - self.created
        
        if diff.days == 0 and diff.seconds >= 0 and diff.seconds < 60:
            return True
        else:
            return False