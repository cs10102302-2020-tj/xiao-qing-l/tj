from django import template
from django.contrib.contenttypes.models import ContentType

from ..models import CollectRecord


register = template.Library()


@register.filter(name='add_arg')
def template_args(instance, arg):
    """stores the arguments in a separate instance attribute"""
    if not hasattr(instance, "_TemplateArgs"):
        setattr(instance, "_TemplateArgs", [])
    instance._TemplateArgs.append(arg)
    return instance


@register.filter(name='call')
def template_method(instance, method):
    """retrieves the arguments if any and calls the method"""
    method = getattr(instance, method)
    if hasattr(instance, "_TemplateArgs"):
        to_return = method(*instance._TemplateArgs)
        delattr(instance, '_TemplateArgs')
        return to_return
    return method()


# 函数名：  get_collect_num
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回obj对应的文章/问题/回答的当前收藏数量
# 输入参数：obj 一个文章/问题/回答的实例化对象
# 返回值：  类型（int）
#          返回obj对应的文章/问题/回答的当前收藏数量
# 修改记录：
@register.simple_tag
def get_collect_num(obj):
    content_type = ContentType.objects.get_for_model(obj)
    object_id = obj.id

    collect_records = CollectRecord.objects.all().filter(content_type=content_type, object_id=object_id)
    collect_num = len(collect_records)
    
    return collect_num
