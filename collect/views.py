from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from collect.models import CollectRecord


# 函数名：  success_response
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回成功状态标识及点赞数量和状态的json响应
# 输入参数：likes_num 该文章的点赞数量
#           is_liked 该用户是否给该文章点赞
# 返回值：  类型（JsonResponse）
#          'status'      : 'SUCCESS'    成功
#          'collect_num' : collect_num  该文章的点赞数量
#          'is_collected': is_collected 该用户是否给该文章点赞
# 修改记录：
def success_response(collect_num, is_collected):
    data = {'status': 'SUCCESS', 'collect_num': collect_num, 'is_collected': is_collected}
    return JsonResponse(data)


# 函数名：  error_response
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回失败状态标识、错误码和错误信息的json响应
# 输入参数：code 错误码
#           message 错误信息
# 返回值：  类型（JsonResponse）
#          'status' : 'ERROR' 失败
#          'code'   : code    错误码
#          'message': message 错误信息
# 修改记录：
def error_response(code, message):
    data = {'status': 'ERROR', 'code': code, 'message': message}
    return JsonResponse(data)


# 函数名：  get_collect_num
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回type和id唯一确定的文章/问题/回答的当前收藏数量
# 输入参数：content_type model类型
#           object_id   model实例的唯一id
# 返回值：  类型（int）
#          返回type和id唯一确定的文章/问题/回答的当前收藏数量
# 修改记录：
def get_collect_num(content_type, object_id):
    collect_records = CollectRecord.objects.all().filter(content_type=content_type, object_id=object_id)
    collect_num = len(collect_records)
    return collect_num


# 函数名：  collect_change
# 作者：    ny
# 日期：    2020-7-19
# 功能：    返回成功状态/失败状态的json响应
# 输入参数：request Django前端传来的请求
# 返回值：  类型（JsonResponse）
#          返回SUCCESS标识的json响应，说明点赞成功，并同时返回收藏数量及状态
#          返回ERROR标识的json响应，说明点赞失败，并同时返回错误码和错误信息
# 修改记录：
@login_required(login_url='/userprofile/login/')
def collect_change(request):
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('collect.change_collectcount'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁\n；如有疑问，请联系管理员！')
    
    # 获取数据和对应的对象
    user = request.user
    content_type = request.GET.get('content_type')
    content_type = ContentType.objects.get(model=content_type)
    object_id = request.GET.get('object_id')

    # 若未收藏，则进行收藏，创建收藏记录
    # 若已收藏，则取消收藏，删除收藏记录
    collect_record, created = CollectRecord.objects.get_or_create(content_type=content_type, object_id=object_id, user=user)
    if not created:
        collect_record.delete()

    collect_num = get_collect_num(content_type, object_id)
    is_collectd = True if created else False
    return success_response(collect_num, is_collectd)